{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}

import Reactive.Banana.Frameworks
import Reactive.Banana
import Graphics.Gloss
import           Graphics.WorldTurtle 
import qualified Graphics.Gloss.Data.Picture as G
import qualified Sound.Tidal.Context as Tidal


input :: IO ()
input = runTurtle $ forward 10

networkDescription :: AddHandler () -> MomentIO ()
networkDescription addHandler = do 
    --(addHandler, fire) <- newAddHandler
    userInput <- fromAddHandler addHandler
    reactimate $ mempty

main :: IO ()
main = do 
    {-
    print "Hello, world"
    let networkDescription :: MomentIO ()
        networkDescription = do 
            turtle <- fromAddHandler $ makeTurtle
            reactimate $ forward 10
    -}
    (addHandler, fire) <- newAddHandler
    network <- compile $ networkDescription addHandler
    actuate network

-- network
    --(addHandler, fire) <- newAddHandler
    --turtle <- makeTurtle
    --var <- fromAddHandler $ addHandler
