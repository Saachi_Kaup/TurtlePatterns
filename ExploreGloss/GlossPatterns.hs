{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module GlossPatterns where

import Prelude hiding (Left, Right)
import           Graphics.Gloss
import           Graphics.Gloss.Data.Picture
import           Graphics.Gloss.Data.ViewPort
import           Graphics.Gloss.Interface.Pure.Game
import           Graphics.Gloss.Interface.IO.Interact
import           Graphics.Gloss.Interface.IO.Animate
import           Graphics.Gloss.Interface.IO.Display
import           Graphics.Gloss.Interface.Pure.Display
import           Control.Concurrent
import           Control.Concurrent.Async
import           Control.Concurrent.MVar
import           Control.Monad
import           IntermediateNotation 
import           GetTidalTime
import qualified Sound.Tidal.Context as Tidal
import           ParseTurtle
import qualified Data.Map as M
import qualified Data.List as L
import           Constants


-- Type synonym to represent the line distance
type LineDistance = Float

-- Function to move the line forward
moveForward :: Float -> LineDistance -> LineDistance
moveForward dt distance = distance + speed * dt

moveBackward :: Float -> LineDistance -> LineDistance
moveBackward dt distance = distance - speed * dt

-- Function to render the line
renderLine :: LineDistance -> Picture
renderLine distance = line [(0, 0), (distance, 0)]
--line makes a function from path to picture. Store prev coordinates and then make new?
--how will it rotate??
-- case wise rendering of path

renderMoveForward :: Point -> LineDistance -> Picture
renderMoveForward (x1, y1) distance = line [(x1, y1), (distance, y1)] --rotate?

renderCommand :: String ->  Float -> IO Picture
renderCommand command fdistance = do 
      putStrLn $ command ++ " by " ++ (show fdistance)
      case command of
         "moveForward"  -> return $ renderMoveForward (0, 0) fdistance
         "moveBackward" -> return $ renderMoveForward (0, 0)  $ -fdistance

--update' :: ViewPort -> Float -> LineDistance ->
--        (Float -> LineDistance -> LineDistance) -> LineDistance
update' :: (Float -> LineDistance -> LineDistance) -> ViewPort -> Float -> LineDistance
           -> LineDistance
update' func _ dt distance = func dt distance

update :: ViewPort -> Float -> LineDistance -> LineDistance
update _ dt distance = moveForward dt distance

-- Helper function to update the viewport
updateViewPort :: (Float -> LineDistance -> LineDistance) -> Float -> LineDistance
updateViewPort func dt = func dt 0 -- Start the line from distance 0

--handleInput :: Event -> a -> a
handleInput :: Event -> LineDistance -> LineDistance
handleInput (EventKey (Char 'w') _ _ _) state = moveForward 10 state
handleInput (EventKey (Char 's') _ _ _) state = moveBackward 10 state
handleInput _ state = state

--handle InputIO
handleInputIO :: String -> LineDistance -> LineDistance
handleInputIO "moveForward" state = moveForward 10 state
handleInputIO "moveBackward" state = moveBackward 10 state
handleInputIO _ state = state

handleInput' :: Event -> Float -> IO Float
handleInput' (EventKey (Char 'w') _ _ _) state = do
    return $ state * (-1)
handleInput' _ state = do 
    return $ state * 1

initialState :: Point
initialState = (0, 0)

updateIO :: Float -> LineDistance -> IO LineDistance
updateIO _ state = do
  -- Read the input from the GHCi prompt
  input <- getLine
  -- Check if the input is "quit", if so, exit the simulation
  if input == "quit"
    then return state
    else do
      -- Process the command and update the state
      let newState = handleInputIO input state
      -- Display the updated state
      display (InWindow "Gloss Demo" (800, 600) (10, 10)) white (renderLine newState)
      -- Continue the simulation loop with the updated state
      updateIO 0 newState

runAnimation :: LineDistance -> String -> IO ()
runAnimation state command = do
  animate
      (InWindow "Gloss Demo" (800, 600) (10, 10))
      white 
      renderLine

updateAnimation :: p -> String -> IO ()
updateAnimation state command = do
    picture <- (renderCommand command 10)
    idThread <- myThreadId
    putStrLn $ "Second" ++ show idThread
    display
        (InWindow "Gloss Demo" (800, 600) (10, 10))
         white 
        --(renderCommand command 10)
        picture
        
--handleInputIOState :: MVar LineDistance -> String -> IO LineDistance
handleInputIOState :: MVar b -> String -> IO b
handleInputIOState stateVar command = do 
    state <- readMVar stateVar
    (updateAnimation state command) --passing state after
    --renderCommand command 10 
    --(runAnimation speed command)
    _a <- myThreadId
    print $ show _a
    return $ state -- should be updated

-- Function to handle user input and update the state interactively
handleInputLoop :: MVar Point -> IO ()
handleInputLoop stateVar = do
  putStrLn "Enter command"
  input <- getLine
  newState <- handleInputIOState stateVar input 
  swapMVar stateVar newState -- can do update somewhere else
  if input /= "quit"
    then handleInputLoop stateVar 
    else return ()

main :: IO ()
main = do
  --_ <- forkIO $ runAnimation stateVar
  putStrLn "Type 'moveForward'or 'moveBackward' to control the simulation, or 'quit' to exit."
  stateVar <- newMVar initialState
  handleInputLoop stateVar
  putStrLn "Animation ended."

--turtle2Picture :: Turtle -> Picture
--turtle2Picture = case turtle of
        --Forward a  -> _a 
        --Left a     -> _
        --Right a    -> _

