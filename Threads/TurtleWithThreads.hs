{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module TurtleThreads where
import           Prelude hiding (Left, Right)
import qualified Graphics.WorldTurtle as G
import qualified Graphics.WorldTurtle.Internal.Turtle as GI
--import           IntermediateNotation
import           Control.Concurrent
import           Control.Concurrent.MVar
import           Graphics.Gloss
import           Graphics.Gloss.Interface.IO.Animate
import qualified Sound.Tidal.Context as T

data Turtle = L    Float 
            | R    Float
            | F    Float

makeThread :: IO (MVar String)
makeThread = do myVariable <- newMVar $ T.fastcat $ map pure [F 10, L 10, R 10]
                threadId <- forkIO $ glossAnimate  myVariable
                --command   <- mVarCommand myVariable 
                --bThreadId <- forkIO $ runTurtle $ command 
                return myVariable

-- add an extra state
-- mvt in tidal now, 
-- start from scratch, move everything into new 
-- add on , new effects - miUgens
glossAnimate myVariable =
  animateIO
        (InWindow
               "Hello World!!"     -- window title
                (400, 150)       -- window size
                (10, 10))        -- window position
        white                    -- background color
        (myAnimate myVariable)     -- picture to display
        controllerSetRedraw

myAnimate :: MVar (T.Pattern Turtle) -> Float -> IO Picture
myAnimate mvPattern t = do
    pat <- readMVar mvPattern
    let values = map (T.value) $ T.queryArc pat (T.Arc 0 1)
    -- draw the first cycle, check the time, draw form there

    return $ drawLines values 

drawLines :: [Turtle] -> Picture
drawLines [] = mempty
drawLines (turtle:ts) = case turtle of
     L a ->   rotate a $ drawLines ts
     R a ->   rotate (0 - a) $ drawLines ts
     F a -> pictures [line [(0, 0), (-a, 0)], drawLines ts]
-- do notaiton
       
myPicture :: String -> Picture
myPicture str = Translate (-170) (-20) -- shift the text to the middle of the window
          $ Scale 0.5 0.5               -- display it half the original size
          $ line [(0, 0), (40, 40)]

{-
turtlePicture :: TurtleCommand () -> TurtleCommand Picture
turtlePicture turtle = representation

mVarCommand :: MVar String -> IO (TurtleCommand ())
mVarCommand mvar = do
    str <- readMVar mvar
    return $ myCommand str

myCommand :: String -> TurtleCommand ()
myCommand str = setRepresentation $ myPicture str


myRepresentation = do
    command <- representation
    --putStrLn $ show command
    return ()
-} 
