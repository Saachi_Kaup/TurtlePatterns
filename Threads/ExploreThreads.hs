-- Import the required modules
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Animate
import Control.Concurrent (forkIO, threadDelay)
import Control.Monad (forever)

-- Define your animation function
animation :: Float -> IO Picture
animation time = do
    return $ line[(0,0), (1,0)] 

-- Function to start the animation in a separate thread
startAnimation :: IO ()
startAnimation = do
  let windowDisplay = InWindow "Animation" (800, 600) (10, 10)
      background    = white
  picture <- animation 10
  forkIO $ animate windowDisplay background (\_ -> picture)
  return ()

-- Function to run GHCi with animation support
runGHCiWithAnimation :: IO ()
runGHCiWithAnimation = do
  putStrLn "Welcome to GHCi with Gloss Animation Support!"
  putStrLn "Type `startAnimation` to start the animation."
  forever $ do
    putStr "> "
    line <- getLine
    putStrLn $ "You entered: " ++ line
    startAnimation

-- Call this function to start GHCi with animation support
main :: IO ()
main = runGHCiWithAnimation

