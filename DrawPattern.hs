{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module DrawPattern where
import           DrawTurtle
import Prelude hiding (Left, Right)
import qualified Graphics.WorldTurtle as WT
import qualified Graphics.Gloss.Data.Picture as G
import qualified Sound.Tidal.Context as Tidal
import           IntermediateNotation
import           GetTidalTime
import           ParseTurtle
import           Control.Concurrent.MVar

multiplyByTime :: Tidal.Time -> Turtle -> Turtle
multiplyByTime time turtle = multiplyT (fromRational time) turtle

multiplyEventsMap :: [(Tidal.Time, Turtle)] -> [Turtle]
multiplyEventsMap = map (\(f, t) -> multiplyByTime f t) 

pattern2Turtles :: Tidal.Pattern Turtle -> [Turtle]
pattern2Turtles pat = multiplyEventsMap eventsMap
       where 
           eventsMap = zip (eventTimes evs) turtles
           turtles = multiplyTByFactors 1 (pi / 2) $ eventsValues evs
           evs = sortEventsByArc pat (makeArc 0 1)

resultpat :: [Turtle]
resultpat = pattern2Turtles $ Tidal.append (Tidal.slowSqueeze "1 3 1" ("[l f, f r]" :: Tidal.Pattern Turtle)) $ Tidal.every "3" (Tidal.fast "2 1") $ Tidal.slow "1 2" tripat

runMVar pat mvar = do
     val <- readMVar mvar
     return $ WT.runTurtle $ sequenceTurtles $ pattern2Turtles $ pat

chaosmap :: Tidal.Pattern Turtle
chaosmap = Tidal.slow "1 1 2 3 5 8" tripat

main :: IO ()
main = do
       let colorShift = WT.penColor >>= WT.setPenColor . WT.shiftHue 1
       WT.runTurtle' WT.black $ do
           WT.setSpeed 500
           WT.setPenColor WT.red
           (intersperseSequence (pattern2Turtles "f") colorShift)
