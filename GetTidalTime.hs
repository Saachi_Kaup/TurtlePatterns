{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module GetTidalTime where

import           Sound.Tidal.Context
import           Data.List
import           Data.Maybe (fromJust)
import           IntermediateNotation
import           ParseTurtle
import qualified Data.Map as M
import           Prelude hiding (Left, Right)


events0To1 :: Pattern a -> [Event a]
events0To1 pat = query (pat) (State (Arc 0 1) (M.empty))

arcEvents :: Pattern a -> Arc -> [Event a]
arcEvents pat arc = queryArc pat arc

sortEvents' :: Ord a => [EventF a b] -> [a]
sortEvents' evs = sort (map (\x -> fromJust $ whole x) evs)

sortEvents :: [EventF Arc b] -> Pattern a -> [Event a]
sortEvents evs pat = concatMap (queryArc pat) (sortEvents' evs)

sortEventsByArc :: Pattern a -> Arc -> [Event a]
sortEventsByArc pat arc = sortEvents (arcEvents pat arc) pat


eventsValues :: [Event b] -> [b]
eventsValues events = map (value) events 

arcTime :: ArcF Time -> Time
arcTime a = stop a - start a

makeArc :: Time -> Time -> Arc
makeArc start end = Arc start end

--eventTimes :: Num a => [EventF (ArcF a) b] -> [a]
eventTimes :: [EventF (ArcF Time) b] -> [Time]
eventTimes events = map (arcTime <$> part) events

--pattern2Turtle :: Pattern a -> [Turtle]
--pattern2Turtle pat = [] 

globalEvents :: [Event Turtle]
globalEvents = sortEvents (events0To1 pat) pat 
