module Constants where

windowWidth, windowHeight :: Int
windowWidth = 800
windowHeight = 600

-- Distance to move the line forward in each step
speed :: Float
speed = 5

